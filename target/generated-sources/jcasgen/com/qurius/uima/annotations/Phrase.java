

/* First created by JCasGen Tue Nov 10 10:28:58 PST 2015 */
package com.qurius.uima.annotations;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.tcas.Annotation;


/** Instance of a cannonical term.
 * Updated by JCasGen Tue Nov 10 10:28:58 PST 2015
 * XML source: /Users/andrewhall/Documents/qurius/dictionary-annotator/target/jcasgen/typesystem.xml
 * @generated */
public class Phrase extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Phrase.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated
   * @return index of the type  
   */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Phrase() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated
   * @param addr low level Feature Structure reference
   * @param type the type of this Feature Structure 
   */
  public Phrase(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated
   * @param jcas JCas to which this Feature Structure belongs 
   */
  public Phrase(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated
   * @param jcas JCas to which this Feature Structure belongs
   * @param begin offset to the begin spot in the SofA
   * @param end offset to the end spot in the SofA 
  */  
  public Phrase(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** 
   * <!-- begin-user-doc -->
   * Write your own initialization here
   * <!-- end-user-doc -->
   *
   * @generated modifiable 
   */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: label

  /** getter for label - gets The word or phrase referenced in the dictionary
   * @generated
   * @return value of the feature 
   */
  public String getLabel() {
    if (Phrase_Type.featOkTst && ((Phrase_Type)jcasType).casFeat_label == null)
      jcasType.jcas.throwFeatMissing("label", "com.qurius.uima.annotations.Phrase");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Phrase_Type)jcasType).casFeatCode_label);}
    
  /** setter for label - sets The word or phrase referenced in the dictionary 
   * @generated
   * @param v value to set into the feature 
   */
  public void setLabel(String v) {
    if (Phrase_Type.featOkTst && ((Phrase_Type)jcasType).casFeat_label == null)
      jcasType.jcas.throwFeatMissing("label", "com.qurius.uima.annotations.Phrase");
    jcasType.ll_cas.ll_setStringValue(addr, ((Phrase_Type)jcasType).casFeatCode_label, v);}    
   
    
  //*--------------*
  //* Feature: score

  /** getter for score - gets The score assigned to the label in the dictionary.
   * @generated
   * @return value of the feature 
   */
  public String getScore() {
    if (Phrase_Type.featOkTst && ((Phrase_Type)jcasType).casFeat_score == null)
      jcasType.jcas.throwFeatMissing("score", "com.qurius.uima.annotations.Phrase");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Phrase_Type)jcasType).casFeatCode_score);}
    
  /** setter for score - sets The score assigned to the label in the dictionary. 
   * @generated
   * @param v value to set into the feature 
   */
  public void setScore(String v) {
    if (Phrase_Type.featOkTst && ((Phrase_Type)jcasType).casFeat_score == null)
      jcasType.jcas.throwFeatMissing("score", "com.qurius.uima.annotations.Phrase");
    jcasType.ll_cas.ll_setStringValue(addr, ((Phrase_Type)jcasType).casFeatCode_score, v);}    
   
    
  //*--------------*
  //* Feature: dictionaryId

  /** getter for dictionaryId - gets Dictionary ID.
   * @generated
   * @return value of the feature 
   */
  public String getDictionaryId() {
    if (Phrase_Type.featOkTst && ((Phrase_Type)jcasType).casFeat_dictionaryId == null)
      jcasType.jcas.throwFeatMissing("dictionaryId", "com.qurius.uima.annotations.Phrase");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Phrase_Type)jcasType).casFeatCode_dictionaryId);}
    
  /** setter for dictionaryId - sets Dictionary ID. 
   * @generated
   * @param v value to set into the feature 
   */
  public void setDictionaryId(String v) {
    if (Phrase_Type.featOkTst && ((Phrase_Type)jcasType).casFeat_dictionaryId == null)
      jcasType.jcas.throwFeatMissing("dictionaryId", "com.qurius.uima.annotations.Phrase");
    jcasType.ll_cas.ll_setStringValue(addr, ((Phrase_Type)jcasType).casFeatCode_dictionaryId, v);}    
  }

    