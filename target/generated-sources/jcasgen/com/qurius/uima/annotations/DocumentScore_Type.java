
/* First created by JCasGen Tue Nov 10 10:28:58 PST 2015 */
package com.qurius.uima.annotations;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.tcas.Annotation_Type;

/** 
 * Updated by JCasGen Tue Nov 10 10:28:58 PST 2015
 * @generated */
public class DocumentScore_Type extends Annotation_Type {
  /** @generated 
   * @return the generator for this type
   */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (DocumentScore_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = DocumentScore_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new DocumentScore(addr, DocumentScore_Type.this);
  			   DocumentScore_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new DocumentScore(addr, DocumentScore_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = DocumentScore.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("com.qurius.uima.annotations.DocumentScore");
 
  /** @generated */
  final Feature casFeat_score;
  /** @generated */
  final int     casFeatCode_score;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public double getScore(int addr) {
        if (featOkTst && casFeat_score == null)
      jcas.throwFeatMissing("score", "com.qurius.uima.annotations.DocumentScore");
    return ll_cas.ll_getDoubleValue(addr, casFeatCode_score);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setScore(int addr, double v) {
        if (featOkTst && casFeat_score == null)
      jcas.throwFeatMissing("score", "com.qurius.uima.annotations.DocumentScore");
    ll_cas.ll_setDoubleValue(addr, casFeatCode_score, v);}
    
  
 
  /** @generated */
  final Feature casFeat_dictionaryId;
  /** @generated */
  final int     casFeatCode_dictionaryId;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public String getDictionaryId(int addr) {
        if (featOkTst && casFeat_dictionaryId == null)
      jcas.throwFeatMissing("dictionaryId", "com.qurius.uima.annotations.DocumentScore");
    return ll_cas.ll_getStringValue(addr, casFeatCode_dictionaryId);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setDictionaryId(int addr, String v) {
        if (featOkTst && casFeat_dictionaryId == null)
      jcas.throwFeatMissing("dictionaryId", "com.qurius.uima.annotations.DocumentScore");
    ll_cas.ll_setStringValue(addr, casFeatCode_dictionaryId, v);}
    
  
 
  /** @generated */
  final Feature casFeat_dictionaryName;
  /** @generated */
  final int     casFeatCode_dictionaryName;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public String getDictionaryName(int addr) {
        if (featOkTst && casFeat_dictionaryName == null)
      jcas.throwFeatMissing("dictionaryName", "com.qurius.uima.annotations.DocumentScore");
    return ll_cas.ll_getStringValue(addr, casFeatCode_dictionaryName);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setDictionaryName(int addr, String v) {
        if (featOkTst && casFeat_dictionaryName == null)
      jcas.throwFeatMissing("dictionaryName", "com.qurius.uima.annotations.DocumentScore");
    ll_cas.ll_setStringValue(addr, casFeatCode_dictionaryName, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	 * @generated
	 * @param jcas JCas
	 * @param casType Type 
	 */
  public DocumentScore_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_score = jcas.getRequiredFeatureDE(casType, "score", "uima.cas.Double", featOkTst);
    casFeatCode_score  = (null == casFeat_score) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_score).getCode();

 
    casFeat_dictionaryId = jcas.getRequiredFeatureDE(casType, "dictionaryId", "uima.cas.String", featOkTst);
    casFeatCode_dictionaryId  = (null == casFeat_dictionaryId) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_dictionaryId).getCode();

 
    casFeat_dictionaryName = jcas.getRequiredFeatureDE(casType, "dictionaryName", "uima.cas.String", featOkTst);
    casFeatCode_dictionaryName  = (null == casFeat_dictionaryName) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_dictionaryName).getCode();

  }
}



    