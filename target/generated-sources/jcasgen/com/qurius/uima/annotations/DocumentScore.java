

/* First created by JCasGen Tue Nov 10 10:28:58 PST 2015 */
package com.qurius.uima.annotations;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.tcas.Annotation;


/** 
 * Updated by JCasGen Tue Nov 10 10:28:58 PST 2015
 * XML source: /Users/andrewhall/Documents/qurius/dictionary-annotator/target/jcasgen/typesystem.xml
 * @generated */
public class DocumentScore extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(DocumentScore.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated
   * @return index of the type  
   */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected DocumentScore() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated
   * @param addr low level Feature Structure reference
   * @param type the type of this Feature Structure 
   */
  public DocumentScore(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated
   * @param jcas JCas to which this Feature Structure belongs 
   */
  public DocumentScore(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated
   * @param jcas JCas to which this Feature Structure belongs
   * @param begin offset to the begin spot in the SofA
   * @param end offset to the end spot in the SofA 
  */  
  public DocumentScore(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** 
   * <!-- begin-user-doc -->
   * Write your own initialization here
   * <!-- end-user-doc -->
   *
   * @generated modifiable 
   */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: score

  /** getter for score - gets Document scored based on weighted dictionary values.
   * @generated
   * @return value of the feature 
   */
  public double getScore() {
    if (DocumentScore_Type.featOkTst && ((DocumentScore_Type)jcasType).casFeat_score == null)
      jcasType.jcas.throwFeatMissing("score", "com.qurius.uima.annotations.DocumentScore");
    return jcasType.ll_cas.ll_getDoubleValue(addr, ((DocumentScore_Type)jcasType).casFeatCode_score);}
    
  /** setter for score - sets Document scored based on weighted dictionary values. 
   * @generated
   * @param v value to set into the feature 
   */
  public void setScore(double v) {
    if (DocumentScore_Type.featOkTst && ((DocumentScore_Type)jcasType).casFeat_score == null)
      jcasType.jcas.throwFeatMissing("score", "com.qurius.uima.annotations.DocumentScore");
    jcasType.ll_cas.ll_setDoubleValue(addr, ((DocumentScore_Type)jcasType).casFeatCode_score, v);}    
   
    
  //*--------------*
  //* Feature: dictionaryId

  /** getter for dictionaryId - gets Dictionary ID.
   * @generated
   * @return value of the feature 
   */
  public String getDictionaryId() {
    if (DocumentScore_Type.featOkTst && ((DocumentScore_Type)jcasType).casFeat_dictionaryId == null)
      jcasType.jcas.throwFeatMissing("dictionaryId", "com.qurius.uima.annotations.DocumentScore");
    return jcasType.ll_cas.ll_getStringValue(addr, ((DocumentScore_Type)jcasType).casFeatCode_dictionaryId);}
    
  /** setter for dictionaryId - sets Dictionary ID. 
   * @generated
   * @param v value to set into the feature 
   */
  public void setDictionaryId(String v) {
    if (DocumentScore_Type.featOkTst && ((DocumentScore_Type)jcasType).casFeat_dictionaryId == null)
      jcasType.jcas.throwFeatMissing("dictionaryId", "com.qurius.uima.annotations.DocumentScore");
    jcasType.ll_cas.ll_setStringValue(addr, ((DocumentScore_Type)jcasType).casFeatCode_dictionaryId, v);}    
   
    
  //*--------------*
  //* Feature: dictionaryName

  /** getter for dictionaryName - gets Dictionary Name.
   * @generated
   * @return value of the feature 
   */
  public String getDictionaryName() {
    if (DocumentScore_Type.featOkTst && ((DocumentScore_Type)jcasType).casFeat_dictionaryName == null)
      jcasType.jcas.throwFeatMissing("dictionaryName", "com.qurius.uima.annotations.DocumentScore");
    return jcasType.ll_cas.ll_getStringValue(addr, ((DocumentScore_Type)jcasType).casFeatCode_dictionaryName);}
    
  /** setter for dictionaryName - sets Dictionary Name. 
   * @generated
   * @param v value to set into the feature 
   */
  public void setDictionaryName(String v) {
    if (DocumentScore_Type.featOkTst && ((DocumentScore_Type)jcasType).casFeat_dictionaryName == null)
      jcasType.jcas.throwFeatMissing("dictionaryName", "com.qurius.uima.annotations.DocumentScore");
    jcasType.ll_cas.ll_setStringValue(addr, ((DocumentScore_Type)jcasType).casFeatCode_dictionaryName, v);}    
  }

    