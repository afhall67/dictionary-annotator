package com.qurius.uima.annotator;

import org.apache.commons.io.FileUtils;
import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.fit.factory.AnalysisEngineFactory;
import org.apache.uima.fit.factory.JCasFactory;
import org.apache.uima.fit.pipeline.SimplePipeline;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.resource.ResourceInitializationException;
import org.junit.Before;
import org.junit.Test;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.IOException;
import java.util.Map;


/**
 * Created by buddha on 11/3/15.
 */
public class DictionaryTest {
    private static String YAML_FILE = ".qubla.yml";
    private String DOCUMENT_TEXT;
    private String DOCUMENT_LANGUAGE;

    @Before
    public void setUp() throws Exception {
        // Nothing to see here, folks, move along...
    }

    @Test
    public void testDictionaryAnnotator() throws UIMAException, IOException {
        AnalysisEngineDescription aed = buildAnalysisEngineWithParameters();

        JCas jCas = JCasFactory.createJCas();
        jCas.setDocumentText(DOCUMENT_TEXT);
        jCas.setDocumentLanguage(DOCUMENT_LANGUAGE);
        SimplePipeline.runPipeline(jCas, new AnalysisEngineDescription[]{aed});

        /* Check the Annotations */
        /* Final score should be 5 + 6 + 2 - 3 = 10 */
        FSIterator iterator = jCas.getAnnotationIndex().iterator();
        while (iterator.hasNext()) {
            Annotation annotation = (Annotation) iterator.next();
            System.out.print("\n *** " + annotation.getCoveredText() + "\n");
            System.out.print(annotation.toString());
        }

    }

    private AnalysisEngineDescription buildAnalysisEngineWithParameters() throws ResourceInitializationException, IOException {
        Yaml yaml = new Yaml();
        String yamlString = FileUtils.readFileToString(new File(YAML_FILE));
        Map<String, Object> config = (Map<String, Object>) yaml.load(yamlString);

        // Set the document text
        Map<String, Object> doc = (Map<String, Object>)config.get("test-document");
        DOCUMENT_TEXT = (String) doc.get("documentText");
        DOCUMENT_LANGUAGE = (String) doc.get("language");

        // Get the Annotator paramters
        Map<String, Object> params = (Map<String, Object>) config.get("parameters");
        Object[] paramsArray = new Object[2 * params.size()];
        int i = 0;
        for (Map.Entry<String, Object> entry : params.entrySet()) {
            Map<String, Object> v = (Map<String, Object>) entry.getValue();
            paramsArray[i] = entry.getKey();
            if (v.containsKey(("isFile")) && (Boolean)v.get("isFile")) {
                paramsArray[(i + 1)] = new File((String) v.get("defaultValue")).getAbsolutePath();
                //System.out.println("Got a boolean!");
            } else {
                paramsArray[(i + 1)] = v.get("defaultValue");
            }
            i += 2;
        }
        AnalysisEngineDescription aed = AnalysisEngineFactory.createEngineDescription(Dictionary.class, paramsArray);
        aed.doFullValidation();

        return aed;
    }

}